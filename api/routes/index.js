var express = require('express');
var router = express.Router();

var ctrlCompany = require('../controller/company.controllers.js');
var ctrlEmployee = require('../controller/employee.controllers.js');

// Company routes
router
  .route('/test')
  .get(ctrlCompany.authenticate,ctrlCompany.test)



router
   .route('/addOneCompany')  
   .post(ctrlCompany.companyAddOne)



router
   .route('/showAllCompanys')  
   .get(ctrlCompany.showAllCompanys)

router
	.route('/login')  
	.post(ctrlCompany.login) 



// Employee routes

router
	.route('/addEmployee')  
	.post(ctrlCompany.login) 	

  

  module.exports = router;